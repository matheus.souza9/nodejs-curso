const { BeforeInsert, BeforeUpdate } = require("typeorm")
const BcryptProvider = require('../../../providers/BCrypt')

class UserSubscriber {
    // @BeforeInsert()
    beforeInsert(event) {
        console.log('-> beforeInsert')
        
        const hashProvider = new BcryptProvider();

        const user = event.entity;

        if (user.password)
            user.password = hashProvider.generateHash(user.password);
    }

    beforeUpdate(event) {
        const hashProvider = new BcryptProvider();

        const business = event.entity;

        if (business.password)
            business.password = hashProvider.generateHash(business.password);
    }
}

module.exports = UserSubscriber