const routes = require('../../presentations/routes/index.js')
const express = require('express');
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('../../swagger.json')
require('dotenv/config')

const app = express();

app.use(express.json());

app.use('/api', routes);
app.get('/', (request, response) => response.redirect('/api/docs'))
app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerFile))

module.exports = app;