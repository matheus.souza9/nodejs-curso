const bcrypt = require('bcryptjs')
const EnvSettings = require('../configs/EnvSettings')

const { hashSync, compareSync } = bcrypt

class HashProvider {
    generateHash(password) {}
    compareHash(password, hashed) {}
}

class BcryptProvider extends HashProvider {
    generateHash(password) {
        return hashSync(password, EnvSettings.HASH_SALT)
    }

    compareHash(password, hashed) {
        return compareSync(password, hashed);
    }
}

module.exports = BcryptProvider