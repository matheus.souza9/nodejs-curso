const IUserRepository = require('../IUserRepository')
const User = require('../../../../entities/User');
const { getRepository, getConnection, Brackets } = require('typeorm');

class UserRepository extends IUserRepository {
    userRepository;

    constructor() {
        super();
        this.userRepository = getRepository(User);
    }

    async create(user) {
        const newTransaction = await this.startTransaction()
        try {
            const newUser = await this.userRepository.create(user);

            await this.userRepository.save(newUser);

            await newTransaction.commitTransaction()
            await newTransaction.release()

            return newUser;
        } catch (error) {
            await newTransaction.rollbackTransaction()
            await newTransaction.release()

            throw new Error(error.message)
        }
    }

    async update(user) {
        const newTransaction = await this.startTransaction()
        try {
            await this.userRepository.save(user);

            await newTransaction.commitTransaction()
            await newTransaction.release()
        } catch (error) {
            await newTransaction.rollbackTransaction()
            await newTransaction.release()

            throw new Error(error.message)
        }
    }

    async delete(id) {
        const newTransaction = await this.startTransaction()
        try {
            await this.userRepository.delete({ id });

            await newTransaction.commitTransaction()
            await newTransaction.release()
        } catch (error) {
            await newTransaction.rollbackTransaction()
            await newTransaction.release()

            throw new Error(error.message)
        }
    }

    async findAll({ query, page, limit }) {
        const qb = this.userRepository.createQueryBuilder('users')

        if (query) {
            qb.where(new Brackets((qb) => {
                qb.where('users.name LIKE :query', { query: `%${query}%` })
                qb.orWhere('users.email LIKE :query', { query: `%${query}%` })
            }))
        }

        if (page && limit) {
            qb.skip((page - 1) * limit)
            qb.take(limit)
        }

        qb.orderBy('users.createdAt', 'DESC')

        const users = await qb.getManyAndCount()

        return users;
    }

    async findById(id) {
        const user = await this.userRepository.findOne({ id });

        return user;
    }

    async findByEmail(email) {
        const user = await this.userRepository.findOne({ email });

        return user;
    }

    async startTransaction() {
        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();

        await queryRunner.connect();

        await queryRunner.startTransaction();

        this.userRepository = queryRunner.manager.getRepository(User);

        return queryRunner;
    }
}

module.exports = UserRepository;