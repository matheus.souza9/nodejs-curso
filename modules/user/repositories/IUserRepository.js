module.exports = class IUserRepository {
    create(user) {}
    update(user) {}
    delete(id) {}
    findAll(params) {}
    findByEmail(email) {}
    findById(id) {}
}