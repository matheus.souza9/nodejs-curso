class UpdateUserController {
    #updateUserService;

    constructor(updateUserService) {
        this.#updateUserService = updateUserService;
    }

    async handle(request, response) {
        const { id } = request.params;
        const { name, email, password } = request.body;

        await this.#updateUserService.execute({ id, name, email, password });

        return response.status(204).send();
    }
}

module.exports = UpdateUserController;