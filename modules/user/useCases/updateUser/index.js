const BcryptProvider = require("../../../../providers/BCrypt");
const UserRepository = require("../../repositories/typeorm/UserRepository");
const UpdateUserController = require("./UpdateUserController");
const UpdateUserService = require("./UpdateUserService");

const UpdateUser = (request, response) => {
    const hashProvider = new BcryptProvider()
    const userRepository = new UserRepository();
    const service = new UpdateUserService(userRepository, hashProvider);
    const controller = new UpdateUserController(service);
    return controller.handle(request, response)
}

module.exports = UpdateUser;