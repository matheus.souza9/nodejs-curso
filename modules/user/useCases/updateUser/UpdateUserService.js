const hashSync = require('bcryptjs').hashSync;

class UpdateUserService {
    userRepository
    hashProvider

    constructor(userRepository, hashProvider) {
        this.userRepository = userRepository
        this.hashProvider = hashProvider
    }

    async execute({ id, name, email, password }) {
        this.validateData({ name, email, password });

        const userExists = await this.userRepository.findById(id);

        if (!userExists) throw new Error('User not found');

        const userExistsWithEmail = await this.userRepository.findByEmail(email);

        if (userExistsWithEmail && userExistsWithEmail.id !== id) throw new Error('User already exists with this email');

        password = this.hashProvider.generateHash(password)

        userExists.name = name;
        userExists.email = email;
        userExists.password = password;

        await this.userRepository.update(userExists);
    }

    validateData(obj) {
        Object.keys(obj).forEach(key => {
            if (!obj[key]) throw new Error(`Missing param: ${key}`);

            if (typeof obj[key] !== 'string') throw new Error(`Invalid param: ${key}`);
        });
    }
}

module.exports = UpdateUserService