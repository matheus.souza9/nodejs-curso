class ListAllUsersService {
    userRepository

    constructor(userRepository) {
        this.userRepository = userRepository
    }

    async execute(params) {
        const users = await this.userRepository.findAll(params)
        return users
    }
}

module.exports = ListAllUsersService;