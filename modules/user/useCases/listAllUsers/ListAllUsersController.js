const PaginationProvider = require("../../../../utils/helpers/Pagination")

class ListAllUsersController {
    listAllUsersService

    constructor(listAllUsersService) {
        this.listAllUsersService = listAllUsersService
    }

    async handle(request, response) {
        const query = request.query.query
        const page = request.query.page || 1
        const limit = request.query.limit || 10

        const users = await this.listAllUsersService.execute({ query, page, limit })

        if (users[0].length === 0) {
            return response.status(204).send()
        }

        const usersWithPagination = PaginationProvider({
            data: users,
            limit,
            page
        })

        return response.status(200).json(usersWithPagination)
    }
}

module.exports = ListAllUsersController