const UserRepository = require('../../repositories/typeorm/UserRepository');
const ListAllUsersController = require("./ListAllUsersController")
const ListAllUsersService = require("./ListAllUsersService")

const ListAllUsers = (request, response)  => {
    const repository = new UserRepository()
    const service = new ListAllUsersService(repository)
    const controller = new ListAllUsersController(service)
    return controller.handle(request, response)
}

module.exports = ListAllUsers