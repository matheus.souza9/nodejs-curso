class GetUserController {
    getUserService

    constructor(getUserService) {
        this.getUserService = getUserService
    }


    async handle(request, response) {
        const { id } = request.params

        try {
            const user  = await this.getUserService.execute(id)

            return response.status(200).json(user)
        } catch(err) {
            return response.status(400).json({ message: err.message })
        }
    }
}

module.exports = GetUserController