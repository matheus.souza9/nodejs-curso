const UserRepository = require('../../repositories/typeorm/UserRepository');
const GetUserController = require('./GetUserController');
const GetUserService = require('./GetUserService');

const GetUser = (request, response) => {
    const repository = new UserRepository()
    const service = new GetUserService(repository)
    const controller = new GetUserController(service)
    return controller.handle(request, response)
}

module.exports = GetUser