class GetUserService {
    userRepository

    constructor(userRepository) {
        this.userRepository = userRepository
    }

    async execute(id) {
        if (!id || typeof id !== 'string') {
            throw new Error('User id is required')
        }

        const user = await this.userRepository.findById(id)

        if (!user) throw new Error('User not found')

        return user
    }
}

module.exports = GetUserService