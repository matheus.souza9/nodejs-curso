const UserRepository = require('../../repositories/typeorm/UserRepository');
const DeleteUserController = require('./DeleteUserController');
const DeleteUserService = require('./DeleteUserService');

const DeleteUser = (request, response) => {
    const repository = new UserRepository()
    const service = new DeleteUserService(repository)
    const controller = new DeleteUserController(service)
    return controller.handle(request, response)
}

module.exports = DeleteUser