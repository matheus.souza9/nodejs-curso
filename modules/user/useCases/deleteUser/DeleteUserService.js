class DeleteUserService {
    userRepository;

    constructor(userRepository) {
        this.userRepository = userRepository;
    }

    async execute(id)  {
        if (!id || typeof id !== 'string') {
            throw new Error('User id is required')
        }

        const userExist = await this.userRepository.findById(id);

        if (!userExist) throw new Error('User not found')

        await this.userRepository.delete(id)
    }
}

module.exports = DeleteUserService