class DeleteUserController {
    deleteUserService
    
    constructor(deleteUserService) {
        this.deleteUserService = deleteUserService
    }

    async handle(request, response) {
        const { id } =  request.params

        try {
            await this.deleteUserService.execute(id)

            return response.status(204).send()
        } catch (error) {
            return  response.status(400).json({ message: error.message })
        }
    }
}

module.exports = DeleteUserController;