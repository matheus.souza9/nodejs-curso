const BcryptProvider = require('../../../../providers/BCrypt');
const UserRepository = require('../../repositories/typeorm/UserRepository');
const CreateUserController = require('./CreateUserController');
const CreateUserService = require('./CreateUserService');

const CreateUser = (request, response) => {
    const hashProvider = new BcryptProvider()
    const repository = new UserRepository();
    const service = new CreateUserService(repository, hashProvider);
    const controller = new CreateUserController(service);
    return controller.handle(request, response);
}

module.exports = CreateUser;