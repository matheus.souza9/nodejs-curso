class CreateUserService {
    userRepository;
    hashProvider;

    constructor(userRepository, hashProvider) {
        this.userRepository = userRepository;
        this.hashProvider = hashProvider;
    }

    async execute({ name, email, password }) {
        this.validateData({ name, email, password });

        const userExistsWithEmail = await this.userRepository.findByEmail(email);
        
        if (userExistsWithEmail) throw new Error('User already exists with this email');

        password = this.hashProvider.generateHash(password)

        const user = await this.userRepository.create({ name, email, password })
            .catch(err => {
                throw new Error('Falha ao cadastrar usuário')
            })

        const { password: hashPassword, ...rest } = user;

        return rest;
    }

    validateData(obj) {
        Object.keys(obj).forEach(key => {
            if (!obj[key]) throw new Error(`Missing param: ${key}`);

            if (typeof obj[key] !== 'string') throw new Error(`Invalid param: ${key}`);
        });
    }
}

module.exports = CreateUserService;