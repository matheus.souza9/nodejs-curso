class CreateUserController {
    createUserService;

    constructor(createUserService) {
        this.createUserService = createUserService;
    }

    async handle(request, response) {
        const { name, email, password } = request.body;

        try {
            const user = await this.createUserService.execute({ name, email, password });

            return response.status(201).json(user);
        } catch (error) {
            return response.status(400).json({ message: error.message });
        }
    }
}

module.exports = CreateUserController;