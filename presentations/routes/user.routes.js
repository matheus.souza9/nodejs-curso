const CreateUser = require("../../modules/user/useCases/createUser");
const DeleteUser = require("../../modules/user/useCases/deleteUser");
const UpdateUser = require("../../modules/user/useCases/updateUser");
const GetUser = require("../../modules/user/useCases/getUser");
const ListAllUsers = require("../../modules/user/useCases/listAllUsers");

const Router = require('express').Router;

const userRoutes = Router();

userRoutes.post('/create', CreateUser);
userRoutes.put('/update/:id', UpdateUser)
userRoutes.delete('/delete/:id', DeleteUser)
userRoutes.get('/:id', GetUser)
userRoutes.get('/', ListAllUsers)


module.exports = userRoutes;