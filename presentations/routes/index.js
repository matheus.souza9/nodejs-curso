const userRoutes = require('./user.routes');

const Router = require('express').Router;

const routes = Router();

routes.use('/users', userRoutes);

module.exports = routes;