class EnvSettings {
    static HASH_SALT = +process.env.HASH_SALT || 8;
    static NODE_ENV = process.env.NODE_ENV || 'development';
}

module.exports = EnvSettings;