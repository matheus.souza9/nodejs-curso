const EntitySchema = require("typeorm").EntitySchema;

const User = new EntitySchema({
    name: 'User',
    tableName: 'users',
    columns: {
        id: {
            type: 'varchar',
            primary: true,
            generated: 'uuid',
            length: '36'
        },
        name: {
            type: 'varchar',
            length: 255,
            nullable: false
        },
        email: {
            type: 'varchar',
            length: 255,
            nullable: false,
            unique: true
        },
        password: {
            type: 'varchar',
            length: 255,
            nullable: false,
            select: false,
        },
        createdAt: {
            name: 'created_at',
            type: 'datetime',
            createDate: true,
        },
        updatedAt: {
            name: 'updated_at',
            type: 'datetime',
            updateDate: true,
        }
    }
})

module.exports = User;